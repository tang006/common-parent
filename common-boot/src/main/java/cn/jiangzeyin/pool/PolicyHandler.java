package cn.jiangzeyin.pool;

/**
 * Created by jiangzeyin on 2017/12/2.
 */
public enum PolicyHandler {
    Caller,
    Abort,
    Discard,
    DiscardOldest
}
